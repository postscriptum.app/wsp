#!/bin/bash
set -e
baseDir=$(readlink -f $(dirname "$BASH_SOURCE")/../..)
suitesDir="$baseDir/tests/suites/wpt"
mkdir -p "$suitesDir/css2-pagination" "$suitesDir/css-page" "$suitesDir/css-gcpm" "$suitesDir/css-break" "$suitesDir/css-multicol"

tmpDir=$(tempfile)
rm $tmpDir && mkdir $tmpDir
zipFile=$tmpDir/wpt-master.zip
cd $tmpDir

curl https://codeload.github.com/web-platform-tests/wpt/zip/master > $zipFile
unzip -qq -o -x $zipFile wpt-master/css/CSS2/pagination/* wpt-master/css/css-page/* wpt-master/css/css-gcpm/* wpt-master/css/css-break/* wpt-master/css/css-multicol/*

wptCssDir="$tmpDir/wpt-master/css"
mkdir -p $tmpDir/css-multicol-support
mv $wptCssDir/css-multicol/support/*20x20.png $wptCssDir/css-multicol/support/swatch-*.png $tmpDir/css-multicol-support
rm -R $wptCssDir/CSS2/pagination/support $wptCssDir/*/support $wptCssDir/*/META.yml $wptCssDir/*/animation $wptCssDir/*/parsing $wptCssDir/*/reference $wptCssDir/*/*-ref.html
grep -lR '<script' $wptCssDir/* | xargs -I µ rm µ

mv $wptCssDir/CSS2/pagination/* "$suitesDir/css2-pagination"
mv $wptCssDir/css-page/* "$suitesDir/css-page"
mv $wptCssDir/css-gcpm/* "$suitesDir/css-gcpm"
mv $wptCssDir/css-break/* "$suitesDir/css-break"
mv $wptCssDir/css-multicol/* "$suitesDir/css-multicol"
rm -Rf "$suitesDir/css-multicol/support" && mv $tmpDir/css-multicol-support "$suitesDir/css-multicol/support"

find $suitesDir/{css-break,css-multicol} -type f | xargs -I µ sed -E -i -e 's/column(s|-[a-z]+):/-ps-column\1:/g' µ
rm $tmpDir -R