#!/bin/bash
baseDir=$(readlink -f $(dirname "$BASH_SOURCE")/../..)
cd "$baseDir"
inkscape website/public/logos/ps.svg -w 16 -h 16 -o website/public/logos/ps-16x.png
inkscape website/public/logos/ps.svg -w 128 -h 128 -o website/public/logos/ps-128x.png
cp website/public/logos/ps-* ext/dist/icons
convert -background none website/public/logos/ps-128x.png -define icon:auto-resize ext/dist/icons/ps.ico
