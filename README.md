# wsp

# postscriptum-wsp

Sets a workspace for developing Postscriptum.

## Postscriptum

Postscriptum is a solution to produce PDF from HTML with the help of CSS Print.

Please refer to [__https://postscriptum.app__](https://postscriptum.app) for a global picture of the project.

## Build

```
npm install -w core
npm install -w cli/common
npm install -w postpdf
npm install
```

## Issues

Please report any issues on the [*postscriptum-core* project](https://gitlab.com/postscriptum.app/core/-/issues).
