module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    plugins: [
        //'import',
        '@typescript-eslint'
    ],
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended'
    ],
    ignorePatterns: ["*.js"],
    rules: {
        "indent": ["error", "tab"],
        "semi": "error",
        "@typescript-eslint/naming-convention": [
            "error",
            {
                "selector": "variableLike",
                "format": ["camelCase", 'UPPER_CASE'],
                "filter": {
                    "regex": "^(\\w+_\\d_\\d)$",
                    "match": false
                }
            }, {
                "selector": "memberLike",
                "format": ["camelCase"],
                "leadingUnderscore": "allow",
                "filter": {
                    "regex": "^(\\w+(-\\w+)+)|(--[\\w-]+)|(Once)$",
                    "match": false
                }
            }, {
                "selector": 'typeLike',
                "format": ['PascalCase']
            }
        ],
        "space-before-function-paren": ["error", {
            "anonymous": "always",
            "named": "never",
            "asyncArrow": "always"
        }],
        "@typescript-eslint/prefer-for-of": "error",
        "@typescript-eslint/explicit-function-return-type": ["error", {
            "allowExpressions": true
        }],
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/no-this-alias": "off",
        "@typescript-eslint/no-empty-interface": "off",
        "@typescript-eslint/no-var-requires": "off",
        "@typescript-eslint/consistent-type-imports": "error",
        "@typescript-eslint/no-require-imports": "off",
        "no-prototype-builtins": "off",
        "no-debugger": "off",
        //"import/extensions": ["error", "always"],
        //"import/order": [ "error", { "groups": ["builtin", "external", "parent", "sibling", "type"] } ]
    }
    /*,
    "rules": {
        "prefer-for-of": true,
        "typedef": [ true, "call-signature", "parameter" ]
    }*/
}
